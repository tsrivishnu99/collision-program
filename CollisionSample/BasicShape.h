#pragma once
#include <iostream>
#include "Vertex.h"
using namespace std;

struct Velocity
{
	float x;
	float y;
};


class BasicShape
{
public:
	virtual void checkBounds();
	virtual void update();
	virtual void getVerticies();

	float time;
	Vertex* mesh;
	int mass;
	float radius;
	float scale;
	float position[3];
	float angle;
	GLuint bufferID;
	Velocity velocity;
	float angularVelocity;
};