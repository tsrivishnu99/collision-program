#version 130

in vec3 vertexPosition;
in vec4 vertexColor;

uniform vec3 trans;
uniform float scale;
uniform float theta;

out vec4 fragmentColor;

void main()
{
	float angle = radians(theta);

	mat4 rz = mat4 (cos(angle), -sin(angle),  0.0,  0.0, 
                    sin(angle),  cos(angle),  0.0,  0.0,
                    0.0,  0.0,  1.0,  0.0,
                    0.0,  0.0,  0.0,  1.0);
					
	mat4 transMat = mat4 (	1.0, 0.0, 0.0, 0.0,
							0.0, 1.0, 0.0, 0.0,
							0.0, 0.0, 1.0, 0.0,
							trans.x, trans.y, trans.z, 1.0);		
							
	mat4 scaleMat = mat4 (	scale, 0.0,		0.0,	0.0,
							0.0,	scale,	0.0,	0.0,
							0.0,	0.0,	scale,	0.0,
							0.0,	0.0,	0.0,	1.0);
							
	gl_Position.xyz = vertexPosition;
	gl_Position.w = 1.0;
	
	gl_Position = transMat * (rz *(scaleMat * gl_Position));

	fragmentColor = vertexColor;
}