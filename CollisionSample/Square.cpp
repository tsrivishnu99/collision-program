#pragma once
#include "Square.h"
#include "Vertex.h"
#include "SquareBase.h"
#include <SDL/SDL.h>


Square::Square()
{
	getVerticies();
	bufferID = 0;
}

Square::Square(float r)
{
	bufferID = 0;
	radius = r;
	angle = 0;
	scale = radius;
	mass = radius * 10;
	getVerticies();
	position[0] = (rand() % 3) - 1;
	position[1] = (rand() % 3) - 1;
	position[2] = 0;
	velocity.x = 5.0f;
	velocity.y = 10.0f;
	angularVelocity = 0.0; //100.0f * position[0];
}

Square::~Square()
{
}

void Square::getVerticies()
{
	if (mesh)
	{
		delete mesh;
	}
	// create and fill a new point array
	mesh = new Vertex[squarePoints.size()];

	vector<Vertex>::iterator it;
	int i = 0;
	for (it = squarePoints.begin(); it != squarePoints.end(); i++, it++) {
		mesh[i] = *it;
		//cout << " x = " << mesh[i].position.x << " y =  " << mesh[i].position.y << "end" << endl;

	}

}

