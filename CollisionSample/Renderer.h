#pragma once
#include "FatalError.h"
#include <vector>
#include <SDL/SDL.h>
#include <GL/glew.h>
#include "GLSLProgram.h"
#include "BasicShape.h"
#define ScreenWidth 1024
#define ScreenHeight 1024

using namespace std;
class Circle;
class Square;
//class GLSLProgram;

struct POS
{
	float x;
	float y;
};

class Renderer
{
private:
	double distanceBetweenCircles(Circle* c1, Circle* c2);
	double distanceBetweenSquares(Square* c1, Square* c2);
	double distanceBetweenObjects(BasicShape* c1, BasicShape* c2);
	void resolveCollision(float &V1, float &V2, int m1, int m2);
	void resolveLinearCollision(BasicShape*, BasicShape*);
	void resolveAngularCollision(BasicShape*, BasicShape*);
	void detectCollision();
	bool isColliding(BasicShape obj1, BasicShape obj2);
	POS getClosestPoint(BasicShape obj1, BasicShape obj2);
	double distance(float x1, float y1, float x2, float y2);

public:
	Renderer();
	~Renderer();
	/*vector<Circle> circles;
	vector<Square> squares;*/
	vector<BasicShape> objects;
	GLSLProgram program;
	SDL_Window* window;
	void insertSquare(float);
	void insertCircle(float);
	void init();
	void draw();
	void update();
};

