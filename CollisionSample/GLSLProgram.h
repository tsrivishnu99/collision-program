#pragma once
#include <string>
#include <GL/glew.h>
//#include"FatalError.h"
#include<fstream>
#include<vector>

class GLSLProgram
{
public:
	GLSLProgram();
	~GLSLProgram();
	GLuint _programID;
	void compileShaders(const std::string& vertexShaderFilePath, const std::string& fragmentShaderFilepath);
	void linkShaders();
	void addAttribute(const std::string& attributeName);
	void use();
	void unuse();

private:

	int _numAttributes;
	//GLuint _programID;
	GLuint _vertexShaderID;
	GLuint _fragmentShaderID;

	void compileShader(const std::string& filePath, GLuint id);
};

