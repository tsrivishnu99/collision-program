#pragma once
#include "IOManager.h"
#include "Renderer.h"
#include "Circle.h"

IOManager::IOManager()
{
	render = new Renderer();
}


IOManager::~IOManager()
{
}

void IOManager::gameLoop()
{
	SDL_Event evnt;

	while (SDL_PollEvent(&evnt) || true)
	{
		switch (evnt.type)
		{
		case SDL_MOUSEMOTION:
			//std::cout << evnt.motion.x << " " << evnt.motion.y << std::endl;
			break;

		case SDL_KEYDOWN:
			switch (evnt.key.keysym.sym)
			{
			case SDLK_1:
				render->insertSquare(2);
				break;
			case SDLK_2:
				render->insertCircle(0.5);
				break;
			}
			break;

		case SDL_QUIT:
			exit(0);
			break;
		}

		render->update();
		render->draw();
	}

}