#include "BasicShape.h"
#include <SDL/SDL.h>

void BasicShape::checkBounds()
{
	if (position[0] + (radius * 0.1) >= 1)
	{
		if (velocity.x > 0)
		{
			velocity.x *= -1;
		}
	}
	else if (position[0] - (radius * 0.1) <= -1)
	{
		if (velocity.x < 0)
		{
			velocity.x *= -1;
		}
	}

	if (position[1] + (radius * 0.1) >= 1)
	{
		if (velocity.y > 0)
		{
			velocity.y *= -1;
		}
	}
	else if (position[1] - (radius * 0.1) <= -1)
	{
		if (velocity.y < 0)
		{
			velocity.y *= -1;
		}
	}
}
void BasicShape::update()
{
	checkBounds();

	float currentTime;
	currentTime = SDL_GetTicks();
	float deltaTime = (currentTime - time) / 100000;
	time = currentTime;

	position[0] += velocity.x * deltaTime * 10;
	position[1] += velocity.y * deltaTime * 10;
	angle += angularVelocity * deltaTime *100;
}
void BasicShape::getVerticies()
{

}
