#pragma once
#include <GL\glew.h>
#include "BasicShape.h"
//#include "Vertex.h"
class Vertex;

class Circle: public BasicShape
{
public:
	Circle();
	Circle(float);
	~Circle();
	virtual void getVerticies() override;
};

