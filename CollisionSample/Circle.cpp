#pragma once
#include "Circle.h"
#include "Vertex.h"
#include "CircleBase.h"
#include <SDL/SDL.h>

Circle::Circle()
{
	getVerticies();
	bufferID = 0;
}

Circle::Circle(float r)
{
	bufferID = 0;
	radius = r;
	scale = radius;
	mass = radius * 10;
	getVerticies();
	position[0] = ( rand() % 3 ) - 1;
	position[1] = (rand() % 3) - 1;
	position[2] = 0;
	velocity.x = 5.0f;
	velocity.y = 10.0f;
	angularVelocity = 0.0f;
}


Circle::~Circle()
{
}

void Circle::getVerticies()
{
	if (mesh)
	{
		delete mesh;
	}
	// create and fill a new point array
	mesh = new Vertex[circlePoints.size()];

	vector<Vertex>::iterator it;
	int i = 0;
	for (it = circlePoints.begin() ; it != circlePoints.end() ; i++ , it++) {
		mesh[i] = *it;
		//cout << " x = " << mesh[i].position.x << " y =  " << mesh[i].position.y << "end" << endl;
	
	}

}
