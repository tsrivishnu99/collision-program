#pragma once
#include "Renderer.h"
#include "Circle.h"
#include "Square.h"
#include "GLSLProgram.h"
#include "CircleBase.h"
#include "SquareBase.h"
#include "Vertex.h"
#include "BasicShape.h"

Renderer::Renderer()
{
	init();
	createCircle();
	createSquare();
}


Renderer::~Renderer()
{
}

void Renderer::insertSquare(float r)
{
	Square square(r);
	glGenBuffers(1, &(square.bufferID));
	int size = squarePoints.size() * sizeof(Vertex);
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, square.bufferID);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));
	//glBindBuffer(GL_ARRAY_BUFFER, square.bufferID);
	glBufferData(GL_ARRAY_BUFFER, size, square.mesh, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	objects.push_back(square);
}


void Renderer::insertCircle(float r)
{
	Circle circle(r);
	glGenBuffers(1, &(circle.bufferID));
	
	int size = circlePoints.size() * sizeof(Vertex);
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, circle.bufferID);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
	glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));
	//glBindBuffer(GL_ARRAY_BUFFER, circle.bufferID);
	glBufferData(GL_ARRAY_BUFFER, size, circle.mesh, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	objects.push_back(circle);
}

void Renderer::init()
{
	SDL_Init(SDL_INIT_EVERYTHING);

	window = SDL_CreateWindow("Collision", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, ScreenWidth, ScreenHeight, SDL_WINDOW_OPENGL);

	SDL_GLContext context = SDL_GL_CreateContext(window);

	if (!context)
	{
		error("Unable to create context");
	}

	GLenum err = glewInit();

	if (err)
	{
		error(" Unable to initialize glew");
	}

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	//================ Initializing Shaders ==================

	program.compileShaders("Shaders\\colorShader.vert", "Shaders\\colorShader.frag");
	program.addAttribute("vertexPosition");
	program.addAttribute("vertexColor");
	program.linkShaders();
}

void Renderer::draw()
{
	program.use();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	GLuint translocation = glGetUniformLocation(program._programID, "trans");
	GLuint scale = glGetUniformLocation(program._programID, "scale");
	GLuint angle = glGetUniformLocation(program._programID, "theta");
	vector<BasicShape>::iterator it;
	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	
	for (it = objects.begin(); it != objects.end(); it++)
	{
		
		glUniform1f(scale, it->scale);
		glUniform1f(angle, it->angle);
		glUniform3f(translocation, it->position[0], it->position[1], it->position[2]);
		glBindBuffer(GL_ARRAY_BUFFER, it->bufferID);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, position));
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*)offsetof(Vertex, color));
		
		glDrawArrays(GL_TRIANGLE_FAN, 0, circlePoints.size());
	}
	
	SDL_GL_SwapWindow(window);
	program.unuse();
}

void Renderer::update()
{
	vector<BasicShape>::iterator it;
	
	detectCollision();
	
	for (it = objects.begin(); it != objects.end(); it++)
	{
		it->update();
	}
}

double Renderer::distanceBetweenObjects(BasicShape* c1, BasicShape* c2)
{
	double distance;
	distance = sqrt((c1->position[0] - c2->position[0])*(c1->position[0] - c2->position[0]) + (c1->position[1] - c2->position[1])*(c1->position[1] - c2->position[1]));
	return distance;
}

double Renderer::distanceBetweenCircles(Circle* c1, Circle* c2)
{
	double distance;
	distance = sqrt((c1->position[0] - c2->position[0])*(c1->position[0] - c2->position[0]) + (c1->position[1] - c2->position[1])*(c1->position[1] - c2->position[1]));
	return distance;
}

double Renderer::distanceBetweenSquares(Square* c1, Square* c2)
{
	double distance;
	distance = sqrt((c1->position[0] - c2->position[0])*(c1->position[0] - c2->position[0]) + (c1->position[1] - c2->position[1])*(c1->position[1] - c2->position[1]));
	return distance;
}

double Renderer::distance(float x1, float y1, float x2, float y2)
{
	return sqrt(((x1 - x2)*(x1-x2)) +((y1 - y2)* (y1-y2)));
}
void Renderer::resolveCollision(float &V1, float &V2, int m1, int m2)
{
	float v1, v2;
	v1 = ((V1*(m1 - m2)) + (2 * m2*V2)) / (m1 + m2);
	v2 = ((V2*(m2 - m1)) + (2 * m1*V1)) / (m1 + m2);
	V1 = v1;
	V2 = v2;

}

void Renderer::resolveLinearCollision(BasicShape* it, BasicShape* xy)
{

	Velocity normal, tangent, unitNormal, unitTangent, t1, t2;

	BasicShape c1, c2;
	float V1, V2, tc1, tc2;


	normal.x = it->position[0] - xy->position[0];
	normal.y = it->position[1] - xy->position[1];

	float magDistance = (it->radius + xy->radius) * 0.1f * sqrt(2);

	//tangential velocities
	tangent.x = -normal.y;
	tangent.y = normal.x;

	//unit normal vector
	unitNormal.x = normal.x / sqrt((normal.x * normal.x) + (normal.y * normal.y));
	unitNormal.y = normal.y / sqrt((normal.x * normal.x) + (normal.y * normal.y));

	it->position[0] = xy->position[0] + magDistance * unitNormal.x;
	it->position[1] = xy->position[1] + magDistance * unitNormal.y;

	//unit tangential vector
	unitTangent.x = -unitNormal.y;
	unitTangent.y = unitNormal.x;

	//tangential components
	tc1 = (unitTangent.x * it->velocity.x) + (unitTangent.y * it->velocity.y);
	tc2 = (unitTangent.x * xy->velocity.x) + (unitTangent.y * xy->velocity.y);

	//velocities in normal
	V1 = (unitNormal.x * it->velocity.x) + (unitNormal.y * it->velocity.y);
	V2 = (unitNormal.x * xy->velocity.x) + (unitNormal.y * xy->velocity.y);

	resolveCollision(V1, V2, it->mass, xy->mass);
	
	it->velocity.x = (V1 * unitNormal.x) + (tc1 * unitTangent.x);
	it->velocity.y = (V1 * unitNormal.y) + (tc1 * unitTangent.y);

	xy->velocity.x = (V2 * unitNormal.x) + (tc2 * unitTangent.x);
	xy->velocity.y = (V2 * unitNormal.y) + (tc2 * unitTangent.y);

}

void Renderer::resolveAngularCollision(BasicShape* a, BasicShape* b)
{
	POS unitNormal, unitTangent;
	POS closestPoint = getClosestPoint(*a, *b);
	
	unitNormal.x = closestPoint.x - a->position[0];
	unitNormal.y = closestPoint.y - a->position[1];

	float mag = sqrt(pow(unitNormal.x, 2) + pow(unitNormal.y, 2));

	//unit Normal from the point of impact for A
	unitNormal.x = unitNormal.x / mag;
	unitNormal.y = unitNormal.y / mag;
	
	//unit vector of the angular velocity exerted for B
	unitTangent.x = -unitNormal.y;
	unitTangent.y = unitNormal.x;

	float w1, w2, i1, i2;

	//Computing the current angular velocity
	w1 = a->angularVelocity * a->radius* 0.1f;
	w2 = b->angularVelocity * a->radius* 0.1f;
	
	//computing the component of the linear velocity to the rotational force
	w1 = w1 + (unitTangent.x * a->velocity.x) + (unitTangent.y * a->velocity.y);
	w2 = w2 + (unitTangent.x * b->velocity.x) + (unitTangent.y * b->velocity.y);

	i1 = a->mass * (distance(closestPoint.x, closestPoint.y, a->position[0], a->position[1]));
	i2 = b->mass * (distance(closestPoint.x, closestPoint.y, b->position[0], b->position[1]));

	/*
	a->velocity.x = a->velocity.x * unitNormal.x;
	a->velocity.y = a->velocity.y * unitNormal.y;

	b->velocity.x = b->velocity.x * (-1)*unitNormal.x;
	b->velocity.y = b->velocity.y * (-1)*unitNormal.y;
	*/

	resolveCollision(w1, w2, i1, i2);
	
	a->angularVelocity = w1/ (a->radius * 0.1f);
	b->angularVelocity = w2 / (b->radius * 0.1f);

}

//===========================================
//
//        a==================b
//        |                  |
//        |                  |
//        |                  |
//        |                  |
//        |                  |
//        c==================d
//
//===========================================

POS Renderer::getClosestPoint(BasicShape obj1, BasicShape obj2)
{
	POS a[2], b[2], c[2], d[2];
	//POS a2, b2, c2, d2;

	POS temp;

	float angle[2];

	angle[0] = (obj1.angle *PI) / 180.0;
	angle[1] = (obj2.angle * PI) / 180.0;

	a[0].x = -(obj1.radius) * 0.1f;// *obj1.scale;
	a[0].y = (obj1.radius) * 0.1f;// *obj1.scale;
	b[0].x = (obj1.radius) * 0.1f;// *obj1.scale;
	b[0].x = (obj1.radius) * 0.1f;// *obj1.scale;
	b[0].y = (obj1.radius) * 0.1f;// *obj1.scale;
	c[0].x = -(obj1.radius) * 0.1f;// *obj1.scale;
	c[0].y = -(obj1.radius) * 0.1f;// *obj1.scale;
	d[0].x = (obj1.radius) * 0.1f;// *obj1.scale;
	d[0].y = -(obj1.radius) * 0.1f;// *obj1.scale;

	a[1].x = -(obj2.radius) * 0.1f;// *obj2.scale;
	a[1].y = (obj2.radius) * 0.1f;// *obj2.scale;
	b[1].x = (obj2.radius) * 0.1f;// *obj2.scale;
	b[1].y = (obj2.radius) * 0.1f;// *obj2.scale;
	c[1].x = -(obj2.radius) * 0.1f;// *obj2.scale;
	c[1].y = -(obj2.radius) * 0.1f;// *obj2.scale;
	d[1].x = (obj2.radius) * 0.1f;// *obj2.scale;
	d[1].y = -(obj2.radius) * 0.1f;// *obj2.scale;

	for (int i = 0; i < 2; i++)
	{
		temp.x = a[i].x;
		temp.y = a[i].y;
		a[i].x = (cos(angle[i]) * temp.x) - (sin(angle[i]) * temp.y);
		a[i].y = (sin(angle[i]) * temp.x) + (cos(angle[i]) * temp.y);

		temp.x = b[i].x;
		temp.y = b[i].y;
		b[i].x = (cos(angle[i]) * temp.x) - (sin(angle[i]) * temp.y);
		b[i].y = (sin(angle[i]) * temp.x) + (cos(angle[i]) * temp.y);

		temp.x = c[i].x;
		temp.y = c[i].y;
		c[i].x = (cos(angle[i]) * temp.x) - (sin(angle[i]) * temp.y);
		c[i].y = (sin(angle[i]) * temp.x) + (cos(angle[i]) * temp.y);

		temp.x = d[i].x;
		temp.y = d[i].y;
		d[i].x = (cos(angle[i]) * temp.x) - (sin(angle[i]) * temp.y);
		d[i].y = (sin(angle[i]) * temp.x) + (cos(angle[i]) * temp.y);
	}

	a[0].x += obj1.position[0];
	a[0].y += obj1.position[1];
	b[0].x += obj1.position[0];
	b[0].y += obj1.position[1];
	c[0].x += obj1.position[0];
	c[0].y += obj1.position[1];
	d[0].x += obj1.position[0];
	d[0].y += obj1.position[1];

	a[1].x += obj2.position[0];
	a[1].y += obj2.position[1];
	b[1].x += obj2.position[0];
	b[1].y += obj2.position[1];
	c[1].x += obj2.position[0];
	c[1].y += obj2.position[1];
	d[1].x += obj2.position[0];
	d[1].y += obj2.position[1];

	POS closestPoint; // of second object

	double dis[4];
	dis[0] = distance(obj1.position[0], obj1.position[1], a[1].x, a[1].y);
	dis[1] = distance(obj1.position[0], obj1.position[1], b[1].x, b[1].y);
	dis[2] = distance(obj1.position[0], obj1.position[1], c[1].x, c[1].y);
	dis[3] = distance(obj1.position[0], obj1.position[1], d[1].x, d[1].y);



	double smallest = dis[0];
	int s = 0;
	for (int i = 1; i < 4; i++)
	{
		if (smallest > dis[i])
		{
			smallest = dis[i];
			s = i;
		}
	}

	switch (s)
	{
	case 0:
		closestPoint.x = a[1].x;
		closestPoint.y = a[1].y;
		break;
	case 1:
		closestPoint.x = b[1].x;
		closestPoint.y = b[1].y;
		break;
	case 2:
		closestPoint.x = c[1].x;
		closestPoint.y = c[1].y;
		break;
	case 3:
		closestPoint.x = d[1].x;
		closestPoint.y = d[1].y;
		break;
	default:
		break;
	}
	return closestPoint;
}

bool Renderer::isColliding(BasicShape obj1, BasicShape obj2)
{
	POS closestpoint = getClosestPoint(obj1, obj2);
	float dis = distance(obj1.position[0], obj1.position[1], closestpoint.x, closestpoint.y);

	return (dis < (obj1.radius*0.1f*sqrt(2)));

	//return ((distanceBetweenObjects(&obj1, &obj2)) <= (obj1.radius + obj2.radius)*0.1f);
}

void Renderer::detectCollision()
{
	vector<BasicShape>::iterator it;
	vector<BasicShape>::iterator xy;

	Velocity normal, tangent, unitNormal, unitTangent, t1, t2;

	BasicShape c1, c2;
	float V1, V2, tc1, tc2;


	for (it = objects.begin(); it != objects.end(); it++)
	{
		for (xy = it + 1; xy != objects.end(); xy++)
		{
			c1 = *it;
			c2 = *xy;
			if (isColliding(*it, *xy))
			{
				resolveAngularCollision(&(*it), &(*xy));
				resolveLinearCollision(&(*it), &(*xy));
				//normal velocities
				//normal.x = it->position[0] - xy->position[0];
				//normal.y = it->position[1] - xy->position[1];

				//float magDistance = (it->radius + xy->radius) * 0.1f * sqrt(2);

				////tangential velocities
				//tangent.x = -normal.y;
				//tangent.y = normal.x;

				////unit normal vector
				//unitNormal.x = normal.x / sqrt((normal.x * normal.x) + (normal.y * normal.y));
				//unitNormal.y = normal.y / sqrt((normal.x * normal.x) + (normal.y * normal.y));

				//it->position[0] = xy->position[0] + magDistance * unitNormal.x;
				//it->position[1] = xy->position[1] + magDistance * unitNormal.y;

				////unit tangential vector
				//unitTangent.x = -unitNormal.y;
				//unitTangent.y = unitNormal.x;

				////tangential components
				//tc1 = (unitTangent.x * it->velocity.x) + (unitTangent.y * it->velocity.y);
				//tc2 = (unitTangent.x * xy->velocity.x) + (unitTangent.y * xy->velocity.y);

				////velocities in normal
				//V1 = (unitNormal.x * it->velocity.x) + (unitNormal.y * it->velocity.y);
				//V2 = (unitNormal.x * xy->velocity.x) + (unitNormal.y * xy->velocity.y);

				//resolveCollision(V1, V2, it->mass, xy->mass);

				//it->velocity.x = (V1 * unitNormal.x) + (tc1 * unitTangent.x);
				//it->velocity.y = (V1 * unitNormal.y) + (tc1 * unitTangent.y);

				//xy->velocity.x = (V2 * unitNormal.x) + (tc2 * unitTangent.x);
				//xy->velocity.y = (V2 * unitNormal.y) + (tc2 * unitTangent.y);

			}
		}
	}
}