#pragma once
#include <GL\glew.h>
#include "BasicShape.h"

class Vertex;


class Square: public BasicShape
{
public:
	Square();
	Square(float);
	~Square();
	virtual void getVerticies() override;
};

