#pragma once
#include "IOManager.h"

int main(int argc, char* argv[])
{
	IOManager ioManager;
	ioManager.gameLoop();

	return 0;
}