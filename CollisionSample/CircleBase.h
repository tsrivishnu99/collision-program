#pragma once
#include <iostream>
#include "Vertex.h"
#include <vector>
#include <cmath>

#define divisions 36
#define PI 3.14159265

using namespace std;

std::vector<Vertex> circlePoints;

void createCircle()
{
	float radius = 0.1;
	circlePoints.clear();
	float theta, delta;
	theta = 0;
	delta = 360 / divisions;
	int i = 0;
	Vertex origin, p1, p2;

	origin.position.x = 0;
	origin.position.y = 0;
	origin.position.z = 0;
	origin.color.r = 255;
	origin.color.g = 0;
	origin.color.b = 0;
	origin.color.a = 255;

	for (i = 0; i < divisions; i++)
	{
		p1.position.x = radius * cos((theta)*PI / 180.0);
		p1.position.y = radius * sin((theta)*PI / 180.0);
		p1.position.z = 0;
		p1.color.r = 0;
		p1.color.g = 255;
		p1.color.b = 0;
		p1.color.a = 255;

		p2.position.x = radius * cos((theta + delta)*PI / 180.0);
		p2.position.y = radius * sin((theta + delta)*PI / 180.0);
		p2.position.z = 0;
		p2.color.r = 0;
		p2.color.g = 0;
		p2.color.b = 255;
		p2.color.a = 255;

		circlePoints.push_back(origin);
		circlePoints.push_back(p1);
		circlePoints.push_back(p2);
		theta += delta;
	}
}
