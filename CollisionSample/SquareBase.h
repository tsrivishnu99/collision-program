#pragma once
#include <iostream>
#include "Vertex.h"
#include <vector>
#include <cmath>

using namespace std;

std::vector<Vertex> squarePoints;

void createSquare()
{
	float radius = 0.1;
	squarePoints.clear();
	int i = 0;
	Vertex origin, p1, p2,p3;

	origin.position.x = 0;
	origin.position.y = 0;
	origin.position.z = 0;
	origin.color.r = 255;
	origin.color.g = 0;
	origin.color.b = 0;
	origin.color.a = 255;

	
	p1.position.x = origin.position.x - radius;
	p1.position.y = origin.position.y - radius;
	p1.position.z = 0;
	p1.color.r = 0;
	p1.color.g = 150;
	p1.color.b = 0;
	p1.color.a = 255;
	
	p2.position.x = origin.position.x + radius;
	p2.position.y = origin.position.y + radius;
	p2.position.z = 0;
	p2.color.r = 0;
	p2.color.g = 150;
	p2.color.b = 0;
	p2.color.a = 255;
	
	p3.position.x = origin.position.x - radius;
	p3.position.y = origin.position.y + radius;
	p3.position.z = 0;
	p3.color.r = rand() % 255;
	p3.color.g = rand() % 255;
	p3.color.b = rand() % 255;
	p3.color.a = 255;

	squarePoints.push_back(p1);
	squarePoints.push_back(p2);
	squarePoints.push_back(p3);

	p1.position.x = origin.position.x - radius;
	p1.position.y = origin.position.y - radius;
	p1.position.z = 0;
	p1.color.r = 0;
	p1.color.g = 150;
	p1.color.b = 0;
	p1.color.a = 255;

	p2.position.x = origin.position.x + radius;
	p2.position.y = origin.position.y - radius;
	p2.position.z = 0;
	p2.color.r = rand() % 255;
	p2.color.g = rand() % 255;
	p2.color.b = rand() % 255;
	p2.color.a = 255;

	p3.position.x = origin.position.x + radius;
	p3.position.y = origin.position.y + radius;
	p3.position.z = 0;
	p3.color.r = 0;
	p3.color.g = 150;
	p3.color.b = 0;
	p3.color.a = 255;

	squarePoints.push_back(p1);
	squarePoints.push_back(p2);
	squarePoints.push_back(p3);

}
